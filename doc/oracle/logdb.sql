CREATE TABLE log (
    "id"                  number       primary key not null,
    "facility"            varchar(16)  not null,
    "severity"            varchar(16)  not null,
    "hostname"            varchar(64)  not null,
    "timestamp"           timestamp    default CURRENT_TIMESTAMP not null,
    "application"         varchar(32)  default null null,
    "message"             varchar(4000) default null null,
    "hmac"                varchar(512) default null null
);

CREATE SEQUENCE log_seq 
    START WITH 1 
    INCREMENT BY 1 
    NOMAXVALUE; 

